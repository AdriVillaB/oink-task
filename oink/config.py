import click
import oink.utils as ocu


@click.command()
@click.option("-n", "--name",
              prompt="Your Name",
              help="Your name, for documentation",
              required=True,
              type=str)
@click.option("-u", "--unicode",
              prompt="Unicode Support",
              help="Whether or not your terminal supports unicode",
              required=True,
              default=True,
              type=bool)
@click.option("-e", "--emojis",
              prompt="Emoji Support",
              help="Whether or not your terminal supports emojis",
              required=True,
              default=True,
              type=bool)
def config(name, unicode, emojis):
    """Configure local Oink Task settings"""
    ocu.print_pig()
    ocu.print_info("Configuring local Oink setup.")
    ocu.write_oink_config_value("user_name", name)
    ocu.write_oink_config_value("unicode", str(unicode))
    ocu.write_oink_config_value("emojis", str(emojis))
    ocu.print_success("Oink configuration saved.")
