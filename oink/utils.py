import random
import rich
from rich.console import Console
import os
import pathlib
import configparser
from enum import Enum
import datetime
import re

from oink import VERSION

OINK_CONFIG_DIR = "oink"
OINK_CONFIG_FILENAME = "oink-config.ini"
OINK_PROJECT_FILENAME = ".oink.md"


console = Console()


class OinkError(Exception):
    pass


def _print_line(header, msg):
    try:
        if get_oink_config_value("emojis").lower() == "true":
            rich.print(":pig:", header, msg)
        elif get_oink_config_value("unicode").lower() == "true":
            rich.print(u"\U0001F416", header, msg)
        else:
            # Sadness.
            rich.print(header, msg)
    except (KeyError, AttributeError):
        rich.print(header, msg)


def print_info(msg):
    _print_line("[[bold blue]INFO[/bold blue]]", msg)


def print_success(msg):
    _print_line("[[bold green]SUCCESS[/bold green]]", msg)


def print_warning(msg):
    _print_line("[[bold yellow]WARN[/bold yellow]]", msg)


def print_error(msg):
    _print_line("[[bold red]ERROR[/bold red]]", msg)


def working():
    return console.status("Working...")


def get_random_oink():
    try:
        if get_oink_config_value("unicode").lower() == "false":
            return "oink"
        else:
            return random.choice(
                ["oink",
                    "oink",
                    "oink",
                    "o͖̯̗̗̐̈́̒̌ï̤̳̼̳̎̎̀n̛͈͕̐ḱ̯͎̦͙̀͋̉",
                    "ŏ̷̭͚̭̪̜͚͜i̴̯̔͌̄̒̈̅̇n̶̨̨͇̬̜͔̠̘͂͑̉̄̈́͝k̴̨̧̡̧̲̮̲͔̍̌͂̈́̆̏ͅ",
                    "Øł₦₭",
                    "𝔬𝔦𝔫𝔨",
                    "ʞuᴉo"])
    except (KeyError, AttributeError):
        return "oink"


def print_pig():
    rich.print("""                                                                                """)
    rich.print("""                        `,~!*****!;:.                                           """)
    rich.print("""                  `"<uADNMMMMMMMMMMMMRE%t*,       -;,                           """)
    rich.print("""     .7wqUy^   :cANMMMMMMMMMMMMMMMMMMMMMMMMg~ ,lhQMMT                           """)
    rich.print("""     5N*,\MA *GMMMMMMMMMMMMMMMMMMMMMMMMMMMN* CNMMMMP,.w9,                       """)  # noqa: W605
    rich.print("""   _`iM1]R5!mMMMMMMMMMMMMMMMMMMMMMMMMMMMMMN,`xuCt\: `KNz                        """)  # noqa: W605
    rich.print("""  _SRqNNMO+RMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMRul|=vuG{ :_          [bold white] ________      """)
    rich.print("""    `::`^SMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMl            [bold white]|        |     """)
    rich.print("""         uMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNARMMMN_           [bold white]|  %s  |     """
               % get_random_oink())
    rich.print("""        `RMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMP"CMMMME*"_        [bold white]| _______|     """)
    rich.print("""        _MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM7       [bold white]|/             """)
    rich.print("""        .NMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM7                      """)
    rich.print("""         mMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMg3;                      """)
    rich.print("""         tMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMRK5ul!_                         """)
    rich.print("""         _NMMMMN~_,,::~";^^!!**\\+<4MMMMR!!^;:,`                                """)
    rich.print("""          uMMMMP    7K9pU.         `DMMMP ,PpKQ`                                """)
    rich.print("""          -0MMMt    .RMMM:          ^NMMu  vMMM;                                """)
    rich.print("""           ;NMM^     *NMM!           lMMc   $MM?                                """)
    rich.print("""            ^**`      ^**_            **_   _**~                                """)
    rich.print("""                                                                                """)


def print_pig_version():
    rich.print("""                                                               v%s""" % (VERSION))


# This function from https://github.com/tensorflow/tensorboard/blob/master/tensorboard/uploader/util.py
# Apache 2 licensed
def get_user_config_directory():
    """Returns a platform-specific root directory for user config settings."""
    # On Windows, prefer %LOCALAPPDATA%, then %APPDATA%, since we can expect the
    # AppData directories to be ACLed to be visible only to the user and admin
    # users (https://stackoverflow.com/a/7617601/1179226). If neither is set,
    # return None instead of falling back to something that may be world-readable.
    if os.name == "nt":
        appdata = os.getenv("LOCALAPPDATA")
        if appdata:
            return appdata
        appdata = os.getenv("APPDATA")
        if appdata:
            return appdata
        return None
    # On non-windows, use XDG_CONFIG_HOME if set, else default to ~/.config.
    xdg_config_home = os.getenv("XDG_CONFIG_HOME")
    if xdg_config_home:
        return xdg_config_home
    return os.path.join(os.path.expanduser("~"), ".config")


def get_oink_user_dir() -> str:
    oink_dir = pathlib.Path(get_user_config_directory(), OINK_CONFIG_DIR)
    # Create the directory if it doesn't exist yet.
    oink_dir.mkdir(parents=True, exist_ok=True)
    return str(oink_dir)


def get_oink_user_config_location() -> str:
    return str(pathlib.Path(get_oink_user_dir(), OINK_CONFIG_FILENAME))


def get_oink_config():
    try:
        config = configparser.ConfigParser()
        config.sections()
        config.read(get_oink_user_config_location())
        return config
    except configparser.Error:
        print_error("Unable to read config file at " + get_oink_user_config_location())
        raise OinkError


def write_oink_config_value(key, value):
    try:
        config = get_oink_config()
    except OinkError:
        pass

    if "default" not in config:
        config["default"] = {}
    config["default"][key] = value

    with open(get_oink_user_config_location(), "w") as configfile:
        config.write(configfile)


def get_oink_config_value(key):
    try:
        config = get_oink_config()
        if "default" not in config:
            return None
        if key not in config["default"]:
            return None
        return config["default"][key]
    except OinkError:
        return None


def is_oink_setup() -> bool:
    try:
        config = get_oink_config()
        if config["default"]["user_name"] is not None:
            return True
        return False
    except OinkError:
        return False
    except KeyError:
        return False


DEFAULT_ABOUT = "This is an Oink project file, used to track tasks for projects. It does not " \
    "require the Oink application to interpret, and can be read as a standard " \
    "Markdown document in any text editor. See https://gitlab.com/TheTwitchy/oink-task " \
    "for full details and application source code."

STATUS_REGEX = r"(TODO|DONE|FLAG)"
DATE_REGEX = r"([\d]{4}\-[\d]{2}\-[\d]{2} [\d]{2}\:[\d]{2}\:[\d]{2}\.[\d]{6})"
TASK_MD_REGEX = r"^\* \{[\d]+\}\(" + STATUS_REGEX + r"\)\s*\[" + DATE_REGEX + r"\] .*$"


class TaskStatus(Enum):
    TODO = "TODO"
    DONE = "DONE"
    FLAG = "FLAG"


class OinkTask():
    def __init__(self, description: str,
                 id=-1,
                 status: TaskStatus = "TODO",
                 created_at: datetime.datetime = datetime.datetime.utcnow()):
        self.id = id
        self.description = description
        self.status = status
        self.created_at = datetime.datetime.utcnow()

    def to_md(self):
        return "* {%d}(%s)[%s] %s" % (self.id, self.status, self.created_at, self.description)

    def from_md(md_str):
        md_str = md_str.strip()
        if not re.match(TASK_MD_REGEX, md_str):
            print_warning("Failed to parse the task input '[bold]%s[/bold]'" % md_str)
            return None
        else:
            id = int(re.search(r"\{[\d]+\}", md_str).group().strip("{}"))
            status = re.search(STATUS_REGEX, md_str).group()
            created_at = re.search(DATE_REGEX, md_str).group()
            description = md_str.split("]", 1)[1].strip()
            new_task_obj = OinkTask(description, id=id, created_at=created_at, status=status)
            return new_task_obj


class OinkProject():
    def __init__(self, name, directory, about=DEFAULT_ABOUT, version=VERSION, tasks=[]):
        self.author = get_oink_config_value("user_name")
        self.directory = directory
        self.name = name
        self.about = about
        self.tasks = tasks
        self.version = version

    def save(self):
        fq_file = pathlib.Path(self.directory, OINK_PROJECT_FILENAME)
        with open(fq_file, "w") as fd:
            fd.write("# %s\n" % self.name)
            fd.write("## AUTHOR\n")
            fd.write("%s\n" % self.author)
            fd.write("\n")
            fd.write("## VERSION\n")
            fd.write("%s\n" % self.version)
            fd.write("\n")
            fd.write("## ABOUT\n")
            fd.write("%s\n" % self.about)
            fd.write("\n")
            fd.write("## TASKS\n")
            for task in self.tasks:
                fd.write(task.to_md() + "\n")
            fd.write("\n")

    def _get_text_from_md_section(md_file_str, section):
        md_file_str_list = md_file_str.split("\n")

        for i in range(len(md_file_str_list)):
            line = md_file_str_list[i].strip()
            if line.startswith("#"):
                line = line.strip("#").strip()
                if line.upper() == section.upper():
                    result = ""
                    i = i + 1
                    for j in range(len(md_file_str_list)):
                        try:
                            line2 = md_file_str_list[i + j]
                            if line2.strip().startswith("#"):
                                return result.strip()
                            else:
                                result += line2 + "\n"
                        except IndexError:
                            # End of the file
                            return result.strip()
        return None

    def _get_title_from_md(md_file_str):
        md_file_str_list = md_file_str.split("\n")
        for i in range(len(md_file_str_list)):
            line = md_file_str_list[i].strip()
            if line.startswith("#"):
                line = line.strip("#").strip()
                return line
        return None

    def load(starting_directory, traverse=True):
        current_dir = starting_directory
        last_dir = ""

        while True:
            # Search for a project file, starting in the current directory and going up a directory until we find it
            # or hit the top.
            fq_file = pathlib.Path(current_dir, OINK_PROJECT_FILENAME)

            if os.path.exists(fq_file) and os.path.isfile(fq_file):
                # Read the project file
                with open(fq_file, "r") as fd:
                    md_file_str = fd.read()
                project_name = OinkProject._get_title_from_md(md_file_str)
                version = OinkProject._get_text_from_md_section(md_file_str, "VERSION")
                about = OinkProject._get_text_from_md_section(md_file_str, "ABOUT")
                tasks = []
                task_str = OinkProject._get_text_from_md_section(md_file_str, "TASKS")
                for line in task_str.split("\n"):
                    line = line.strip()
                    # Avoid empty lines
                    if not line == "":
                        task_obj = OinkTask.from_md(line)
                        if task_obj is not None:
                            tasks.append(task_obj)

                project = OinkProject(project_name, current_dir, about=about, version=version, tasks=tasks)
                return project

            if not traverse:
                return None

            if current_dir == pathlib.Path.home() or current_dir == last_dir:
                return None
            last_dir = current_dir
            current_dir = os.path.dirname(current_dir)

    def add_task(self, task):
        if task.id == -1:
            highest_id = -1
            for existing_task in self.tasks:
                if existing_task.id > highest_id:
                    highest_id = existing_task.id
            task.id = highest_id + 1
        self.tasks.append(task)
