import click

import oink.utils as ocu
from oink.project import project
from oink.config import config
from oink.task import new, tasks, status


def print_version(ctx, param, value):
    if not value or ctx.resilient_parsing:
        return
    ocu.print_pig()
    ocu.print_pig_version()
    ctx.exit()


@click.group()
@click.option("-v", "--version", is_flag=True, callback=print_version, expose_value=False, is_eager=True)
def oink_cli():
    pass


oink_cli.add_command(config)
oink_cli.add_command(project)
oink_cli.add_command(new)
oink_cli.add_command(tasks)
oink_cli.add_command(status)

if __name__ == "__main__":
    oink_cli()
