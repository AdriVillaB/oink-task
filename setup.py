import setuptools
from oink import VERSION

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="Oink Task",
    author="TheTwitchy",
    version=VERSION,
    author_email="thetwitchy@thetwitchy.com",
    description="Project-oriented task tracking for weirdos.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/TheTwitchy/oink-task",
    packages=setuptools.find_packages(),
    python_requires=">=3.7",
    install_requires=[
        "click",
        "rich",
    ],
    entry_points={
        "console_scripts": [
            "oink = oink.main:oink_cli",
        ],
    },
)
